��                �      �     �       2        R     l     z     �     �     �     �     �     �     �     �     �     �       	        !     &     7     E     K     f     r     �     �     �     �  �  �     �  %   �  A   �  !   �          0     D  	   J     T     ]     l     z     �     �     �     �     �  	   �     �     �     
  	        &     B     O     e      m     �     �   Add Add document "%s" to cabinets Add document to cabinets Add documents to cabinets Add documents to cabinets Add new level Add to cabinets All Cabinet Cabinets Create cabinet Create cabinets Delete Delete cabinets Details Details of cabinet: %s Document cabinet Document cabinets Documents Edit Edit cabinet: %s Edit cabinets Label List of children cabinets. Navigation: Parent and Label Remove Remove documents from cabinets Remove from cabinets View cabinets Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-05-28 15:49-0400
PO-Revision-Date: 2017-09-27 08:38+0000
Last-Translator:   <admin@itopen.it>
Language-Team: Italian (https://www.transifex.com/rosarior/teams/13584/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Translated-Using: django-rosetta 0.7.13
 Aggiungi Aggiungi documento "%s" agli schedari Aggiungi documento agli schedari Aggiungi documenti agli schedari Aggiungi documenti allo schedario Aggiungi nuovo livello Aggiungi a schedari Tutti Schedario Schedari Crea schedario Crea schedari Elimina Elimina schedari Dettagli Dettagli dello schedario: %s Schedario documento Schedari documento Documenti Modifica Modifica schedario: %s Modifica schedari Etichetta Elenco degli schedari figli Navigazione: Genitore ed etichetta Rimuovi Rimuovi documenti dagli schedari Rimuovi dagli schedari Visualizza schedari 