# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-28 15:49-0400\n"
"PO-Revision-Date: 2017-09-27 08:38+0000\n"
"Last-Translator:   <admin@itopen.it>\n"
"Language-Team: Italian (https://www.transifex.com/rosarior/teams/13584/it/)\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Translated-Using: django-rosetta 0.7.13\n"

#: apps.py:33 forms.py:30 links.py:21 menus.py:8 models.py:34 permissions.py:7
#: views.py:153
msgid "Cabinets"
msgstr "Schedari"

#: links.py:27 links.py:38
msgid "Remove from cabinets"
msgstr "Rimuovi dagli schedari"

#: links.py:31 links.py:35
msgid "Add to cabinets"
msgstr "Aggiungi a schedari"

#: links.py:55
msgid "Add new level"
msgstr "Aggiungi nuovo livello"

#: links.py:60 views.py:39
msgid "Create cabinet"
msgstr "Crea schedario"

#: links.py:64
msgid "Delete"
msgstr "Elimina"

#: links.py:67
msgid "Edit"
msgstr "Modifica"

#: links.py:71
msgid "All"
msgstr "Tutti"

#: links.py:74
msgid "Details"
msgstr "Dettagli"

#: models.py:22
msgid "Label"
msgstr "Etichetta"

#: models.py:25
msgid "Documents"
msgstr "Documenti"

#: models.py:33 models.py:66 serializers.py:134
msgid "Cabinet"
msgstr "Schedario"

#: models.py:67 serializers.py:135
msgid "Parent and Label"
msgstr "Genitore ed etichetta"

#: models.py:74 serializers.py:141
#, python-format
msgid "%(model_name)s with this %(field_labels)s already exists."
msgstr ""

#: models.py:86
msgid "Document cabinet"
msgstr "Schedario documento"

#: models.py:87
msgid "Document cabinets"
msgstr "Schedari documento"

#: permissions.py:12
msgid "Add documents to cabinets"
msgstr "Aggiungi documenti allo schedario"

#: permissions.py:15
msgid "Create cabinets"
msgstr "Crea schedari"

#: permissions.py:18
msgid "Delete cabinets"
msgstr "Elimina schedari"

#: permissions.py:21
msgid "Edit cabinets"
msgstr "Modifica schedari"

#: permissions.py:24
msgid "Remove documents from cabinets"
msgstr "Rimuovi documenti dagli schedari"

#: permissions.py:27
msgid "View cabinets"
msgstr "Visualizza schedari"

#: serializers.py:20
msgid "List of children cabinets."
msgstr "Elenco degli schedari figli"

#: serializers.py:23
msgid "Number of documents on this cabinet level."
msgstr ""

#: serializers.py:27
msgid "The name of this cabinet level appended to the names of its ancestors."
msgstr ""

#: serializers.py:33
msgid ""
"URL of the API endpoint showing the list documents inside this cabinet."
msgstr ""

#: serializers.py:69 serializers.py:175
msgid "Comma separated list of document primary keys to add to this cabinet."
msgstr ""

#: serializers.py:154
msgid ""
"API URL pointing to a document in relation to the cabinet storing it. This "
"URL is different than the canonical document URL."
msgstr ""

#: templates/cabinets/cabinet_details.html:21
msgid "Navigation:"
msgstr "Navigazione:"

#: views.py:70
#, python-format
msgid "Add new level to: %s"
msgstr ""

#: views.py:83
#, python-format
msgid "Delete the cabinet: %s?"
msgstr ""

#: views.py:111
#, python-format
msgid "Details of cabinet: %s"
msgstr "Dettagli dello schedario: %s"

#: views.py:142
#, python-format
msgid "Edit cabinet: %s"
msgstr "Modifica schedario: %s"

#: views.py:177
#, python-format
msgid "Cabinets containing document: %s"
msgstr ""

#: views.py:188
#, python-format
msgid "Add to cabinet request performed on %(count)d document"
msgstr ""

#: views.py:191
#, python-format
msgid "Add to cabinet request performed on %(count)d documents"
msgstr ""

#: views.py:198
msgid "Add"
msgstr "Aggiungi"

#: views.py:200
msgid "Add document to cabinets"
msgid_plural "Add documents to cabinets"
msgstr[0] "Aggiungi documento agli schedari"
msgstr[1] "Aggiungi documenti agli schedari"

#: views.py:211
#, python-format
msgid "Add document \"%s\" to cabinets"
msgstr "Aggiungi documento \"%s\" agli schedari"

#: views.py:222
msgid "Cabinets to which the selected documents will be added."
msgstr ""

#: views.py:250
#, python-format
msgid "Document: %(document)s is already in cabinet: %(cabinet)s."
msgstr ""

#: views.py:260
#, python-format
msgid "Document: %(document)s added to cabinet: %(cabinet)s successfully."
msgstr ""

#: views.py:272
#, python-format
msgid "Remove from cabinet request performed on %(count)d document"
msgstr ""

#: views.py:275
#, python-format
msgid "Remove from cabinet request performed on %(count)d documents"
msgstr ""

#: views.py:282
msgid "Remove"
msgstr "Rimuovi"

#: views.py:284
msgid "Remove document from cabinets"
msgid_plural "Remove documents from cabinets"
msgstr[0] ""
msgstr[1] ""

#: views.py:295
#, python-format
msgid "Remove document \"%s\" to cabinets"
msgstr ""

#: views.py:306
msgid "Cabinets from which the selected documents will be removed."
msgstr ""

#: views.py:333
#, python-format
msgid "Document: %(document)s is not in cabinet: %(cabinet)s."
msgstr ""

#: views.py:343
#, python-format
msgid "Document: %(document)s removed from cabinet: %(cabinet)s."
msgstr ""
