��    '      T      �      �     �     �     �  0   �     �     �     
            
   /     :     A  2   M  6   �     �     �  	   �     �     �     �  	   	  '        ;     A  #   I     m  
   t          �     �  ?   �  @   �     +     J     O     e  	   z  #   �  �  �     d     j     p  ?   �  #   �     �     �               3     C     L  <   `  D   �     �     �  	   	     	     4	     =	     T	  6   i	  	   �	  
   �	  -   �	     �	     �	     �	     
  
   /
  P   :
  F   �
  '   �
  	   �
          #     ;  "   R   All Attach Attach tags Attach tags to document Attach tags to documents Attach tags to document: %s Attach tags to documents Color Create new tag Create new tags Create tag Delete Delete tags Delete the selected tag? Delete the selected tags? Document "%(document)s" is already tagged as "%(tag)s" Document tag Document tags Documents Documents with the tag: %s Edit Edit tag: %s Edit tags Error deleting tag "%(tag)s": %(error)s Label Preview Primary key of the tag to be added. Remove Remove tag Remove tags Remove tags from documents Tag Tag "%(tag)s" attached successfully to document "%(document)s". Tag "%(tag)s" removed successfully from document "%(document)s". Tag "%s" deleted successfully. Tags Tags for document: %s Tags to be attached. View tags Will be removed from all documents. Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-05-28 15:51-0400
PO-Revision-Date: 2017-09-27 09:02+0000
Last-Translator:   <admin@itopen.it>
Language-Team: Italian (http://www.transifex.com/rosarior/mayan-edms/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Translated-Using: django-rosetta 0.7.13
 Tutti Metti Aggiungi etichetta Aggiungi etichette al documento Aggiungi etichette ai documenti Aggiungi etichette al documento: %s Etichettare i documenti Colori Crea nuova etichetta Crea un nuova etichetta Crea etichetta  Cancella Eliminare etichette Cancellare il tag selezionato? Cancellare i tag selezionati? Il documento "%(document)s" è stato già etichettato come "%(tag)s" Etichetta documento  Etichette documento  Documenti Documenti con l'etichetta: %s Modifica Modifica etichetta: %s Modificare etichette Errore nel cancellare l'etichetta "%(tag)s": %(error)s Etichetta Anteprima  Chiave primaria dell'etichetta da aggiungere  Rimuovi Rimuovi etichetta Rimuovi etichette Rimuovi etichetta dal documento Etichetta  L'etichetta "%(tag)s" è stata allegata con successo al documento "%(document)s" Etichetta "%(tag)s" rimossa con successo dal documento "%(document)s". Etichetta "%s" cancellata con successo. Etichette Etichette per il documento: %s Etichette da aggiungere Visualizzare etichette Sarà rimossa da tutti i documenti 